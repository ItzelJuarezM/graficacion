package fractales;
import java.awt.*;
import javax.swing.*;

public class Fractales06 extends JApplet
{
    private int x;
    public void init()
    {
      
        JRootPane rootPane = this.getRootPane();    
        rootPane.putClientProperty("defeatSystemEventQueueCheck", Boolean.TRUE);

    }

    public void paint(Graphics g)
    {

        square(g,200,200,200, 5);
    }
    
    public void square(Graphics g,int x, int y, int tamaño, int veces){

        if(veces == 0){
            g.drawRect(x,y,tamaño,tamaño);
        }else{
            square(g,x+(tamaño/3),(y-tamaño)+(tamaño/3),tamaño/3,veces-1);
            square(g,(x+tamaño)+(tamaño/3),(y-tamaño)+(tamaño/3),tamaño/3,veces-1);
            square(g,(x+tamaño)+(tamaño/3),y+(tamaño/3),tamaño/3,veces-1);
            square(g,(x+tamaño)+(tamaño/3),(y+tamaño)+(tamaño/3),tamaño/3,veces-1);
            square(g,x+(tamaño/3),(y+tamaño)+(tamaño/3),tamaño/3,veces-1);
            square(g,(x-tamaño)+(tamaño/3),(y+tamaño)+(tamaño/3),tamaño/3,veces-1);
            square(g,(x-tamaño)+(tamaño/3),y+(tamaño/3),tamaño/3,veces-1);
            square(g,(x-tamaño)+(tamaño/3),(y-tamaño)+(tamaño/3),tamaño/3,veces-1);
        }
    }
}