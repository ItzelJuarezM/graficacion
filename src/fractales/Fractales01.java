
package fractales;
 import java.awt.*;
 import javax.swing.*;

/**
 *
 * @author daira
 */
public class Fractales01{

public static void main(String[] args) 
{   
    MyCanvas canvas1 = new MyCanvas();
    //La ventana en donde estara el fractal
    JFrame frame = new JFrame();
    frame.setTitle("Circulo fractal");
    frame.setSize(500,500);
    frame.setLocation(100,100);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //se agrega a la ventana como panel contenido
    frame.getContentPane().add(canvas1);
    frame.setVisible(true);
}
 }

 class MyCanvas extends Canvas
 {
public MyCanvas()
{} //constructor

//dibuja el primer circulo y una linea invicible
public void paint (Graphics graphics)
{
    int x1,x2,y1,y2; //coordenadas x y 
    int radius=125; //radio del primer circulo
    int xMid=250, yMid=250; //punto central del circulo
    graphics.drawLine(0,250,500,250);//linea invicible
    graphics.drawOval(xMid-radius,yMid-radius,radius*2,radius*2);//se dibuja el primer circulo
    //run fractal algorithm to draw 2 circles to the left and right
   drawCircles(graphics, xMid, yMid, radius);//Para ejecutar el algoritmo fractal para dibujar circulos a la derecha e izquierda
}

void drawCircles (Graphics graphics, int xMid, int yMid, int radius)
{
    //para colocar circulos a la derecha e izquierda
    int x1 = xMid-radius-(radius/2);
    int y1 = yMid-(radius/2);
    int x2 = xMid+radius-(radius/2);
    int y2= yMid-(radius/2);

    if (radius > 5)
    {
         //Se dibuja cisculo izquierdo y derecho respectivamente
    graphics.drawOval(x1, y1, (radius/2)*2, (radius/2)*2);
    graphics.drawOval(x2, y2, (radius/2)*2, (radius/2)*2);
    }
    drawCircles (graphics, xMid, yMid, radius/2);  
}
 }